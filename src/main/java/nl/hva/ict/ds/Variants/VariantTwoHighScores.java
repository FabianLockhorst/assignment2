package nl.hva.ict.ds.Variants;

import nl.hva.ict.ds.FabianSortUtils;
import nl.hva.ict.ds.HighScoreList;
import nl.hva.ict.ds.Player;

import java.util.ArrayList;
import java.util.List;

public class VariantTwoHighScores implements HighScoreList {
    private static final int BUCKET_SIZE = 50;
    private List<List<Player>> buckets;
    private long maxValue = 0;

    public VariantTwoHighScores() {
        buckets = new ArrayList<>(BUCKET_SIZE);
        for (int i = 0; i < BUCKET_SIZE; i++) {
            buckets.add(new ArrayList<>());
        }
    }

    @Override
    public void add(Player player) {
        if (player.getHighScore() > maxValue) {
            maxValue = player.getHighScore();
        }
        int bucketIndex = (int) (player.getHighScore() * BUCKET_SIZE / (maxValue + 1));
        buckets.get(bucketIndex).add(player);

        FabianSortUtils.InsertionSort(buckets.get(bucketIndex));
    }

    @Override
    public List<Player> getHighScores(int numberOfHighScores) {
        ArrayList<Player> output = createHighScoreList();
        return output.subList(0, Math.min(numberOfHighScores, output.size()));
    }

    @Override
    public List<Player> findPlayer(String firstName, String lastName) throws IllegalArgumentException {
        return FabianSortUtils.getPlayers(firstName, lastName, createHighScoreList());
    }

    private ArrayList<Player> createHighScoreList() {
        ArrayList<Player> players = new ArrayList<>();
        for (int i = BUCKET_SIZE - 1; i >= 0; i--) {
            players.addAll(buckets.get(i));
        }
        return players;
    }
}
