package nl.hva.ict.ds.Variants;

import nl.hva.ict.ds.FabianSortUtils;
import nl.hva.ict.ds.HighScoreList;
import nl.hva.ict.ds.Player;

import java.util.ArrayList;
import java.util.List;

public class VariantOneHighScores implements HighScoreList {
    private ArrayList<Player> players = new ArrayList<>();

    @Override
    public void add(Player player) {
        players.add(player);
        FabianSortUtils.InsertionSort(players);
    }

    @Override
    public List<Player> getHighScores(int numberOfHighScores) {
        return players.subList(0, Math.min(numberOfHighScores, players.size()));
    }

    @Override
    public List<Player> findPlayer(String firstName, String lastName) throws IllegalArgumentException {
        return FabianSortUtils.getPlayers(firstName, lastName, players);
    }
}
