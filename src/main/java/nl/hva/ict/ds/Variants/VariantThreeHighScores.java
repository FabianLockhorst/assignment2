package nl.hva.ict.ds.Variants;

import nl.hva.ict.ds.FabianPriorityQueue;
import nl.hva.ict.ds.FabianSortUtils;
import nl.hva.ict.ds.HighScoreList;
import nl.hva.ict.ds.Player;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class VariantThreeHighScores implements HighScoreList {
    private FabianPriorityQueue players = new FabianPriorityQueue();

    @Override
    public void add(Player player) {
        players.insert(player);
    }

    @Override
    public List<Player> getHighScores(int numberOfHighScores) {
        ArrayList<Player> output = new ArrayList<>();
        int counter = 0;
        while (players.size() != 0 && counter < numberOfHighScores) {
            output.add(players.delMax());
            counter++;
        }
        return output;
    }

    @Override
    public List<Player> findPlayer(String firstName, String lastName) throws IllegalArgumentException {
        return FabianSortUtils.getPlayers(firstName, lastName, Arrays.asList((Player[]) players.toArray()));
    }
}

