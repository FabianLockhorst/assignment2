package nl.hva.ict.ds;

import java.util.Arrays;
import java.util.Iterator;

@SuppressWarnings({"unused", "UnusedReturnValue"})
public class FabianPriorityQueue {
    private Player[] list = new Player[0];

    public FabianPriorityQueue() {
    }

    public Iterator<Player> iterator() {
        return Arrays.asList(list).iterator();
    }

    public int size() {
        return list.length;
    }

    public boolean insert(Player player) {
        Player[] tempList = new Player[list.length + 1];
        System.arraycopy(list, 0, tempList, 0, list.length);
        tempList[tempList.length - 1] = player;
        list = tempList;
        sort();
        return true;
    }

    public Player delMax() {
        if (list.length > 0) {
            Player output = list[0];
            Player[] tempList = new Player[list.length-1];
            System.arraycopy(list, 1, tempList, 0, tempList.length);
            list = tempList;
            sort();
            return output;
        }
        return null;
    }

    public Player max() {
        return list[0];
    }

    private void sort() {
        Player[] array = list;
        for (int i = array.length / 2 - 1; i >= 0; i--) {
            heap(array, array.length, i);
        }
        list = array;
    }

    private void heap(Player[] array, int heapSize, int node) {
        int largestNode = node;
        int leftNode = 2 * node + 1;
        int rightNode = 2 * node + 2;

        if (leftNode < heapSize && array[leftNode].getHighScore() > array[largestNode].getHighScore()) {
            largestNode = leftNode;
        }
        if (rightNode < heapSize && array[rightNode].getHighScore() > array[largestNode].getHighScore()) {
            largestNode = rightNode;
        }

        if (largestNode != node) {
            swapElements(array, node, largestNode);
            heap(array, heapSize, largestNode);
        }
    }

    private void swapElements(Player[] array, int node, int swapNode) {
        Player tempNode = array[node];
        array[node] = array[swapNode];
        array[swapNode] = tempNode;
    }

    public Player[] toArray() {
        return list;
    }
}
