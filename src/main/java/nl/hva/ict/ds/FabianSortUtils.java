package nl.hva.ict.ds;

import java.util.ArrayList;
import java.util.List;

public class FabianSortUtils {

    public static void InsertionSort(List<Player> players) {
        for (int index = 0; index < players.size() - 1; index++) {
            int counter = index;
            while (counter >= 0 && players.get(counter + 1).getHighScore() > players.get(counter).getHighScore()) {
                Player tempPlayer = players.get(counter);
                players.set(counter, players.get(counter + 1));
                players.set(counter + 1, tempPlayer);
                counter--;
            }
        }
    }

    public static List<Player> getPlayers(String firstName, String lastName, List<Player> players) throws IllegalArgumentException {
        if (firstName.isEmpty()) {
            throw new IllegalArgumentException("Firstname may not be empty");
        }
        if (lastName.isEmpty()) {
            throw new IllegalArgumentException("LastName may not be empty");
        }
        List<Player> matchedPlayers = new ArrayList<>();
        for (Player player : players) {
            if ((player.getFirstName().equals(firstName) && player.getLastName().equals(lastName)) ||
                    (player.getFirstName().equals(firstName) || player.getLastName().equals(lastName))) {
                matchedPlayers.add(player);
            }
        }
        return matchedPlayers;
    }
}
