package nl.hva.ict.ds;

import nl.hva.ict.ds.Variants.VariantThreeHighScores;
import org.junit.Before;
import org.junit.Test;

import java.security.SecureRandom;
import java.util.List;
import java.util.Random;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * This class contains some unit tests. They by no means ensure that all the requirements are implemented
 * correctly.
 */
public class HighScoreListTest {
    private static final int MAX_HIGH_SCORE = 100000;
    private Random randomizer = new SecureRandom();
    private HighScoreList highScores;
    private Player nearlyHeadlessNick;
    private Player dumbledore;

    @Before
    public void setup() {
        // Here you should select your implementation to be tested.
        createHighScores();

        nearlyHeadlessNick = new Player("Nicholas", "de Mimsy-Porpington", getHighScore() % 200);
        dumbledore = new Player("Albus", "Dumbledore", nearlyHeadlessNick.getHighScore() * 1000);
    }

    private void createHighScores() {
        highScores = new VariantThreeHighScores();
    }


    @Test
    public void noPlayerNoHighScore() {
        assertTrue("There are high-score while there should be no high-scores!", highScores.getHighScores(1).isEmpty());
    }

    @Test
    public void whenNoHighScoreIsAskedForNonShouldBeGiven() {
        highScores.add(dumbledore);

        assertEquals(0, highScores.getHighScores(0).size());
    }

    @Test
    public void noMoreHighScoresCanBeGivenThenPresent() {
        highScores.add(nearlyHeadlessNick);
        highScores.add(dumbledore);

        assertEquals(2, highScores.getHighScores(10).size());
    }

    @Test
    public void keepAllHighScores() {
        highScores.add(nearlyHeadlessNick);
        highScores.add(dumbledore);

        assertEquals(2, highScores.getHighScores(2).size());
    }

    @Test
    public void singlePlayerHasHighScore() {
        highScores.add(dumbledore);

        assertEquals(dumbledore, highScores.getHighScores(1).get(0));
    }

    @Test
    public void harryBeatsDumbledore() {
        highScores.add(dumbledore);
        Player harry = new Player("Harry", "Potter", dumbledore.getHighScore() + 1);
        highScores.add(harry);

        assertEquals(harry, highScores.getHighScores(1).get(0));
    }

    // Extra unit tests go here

    private long getHighScore() {
        return randomizer.nextInt(MAX_HIGH_SCORE);
    }

    @Test
    public void performanceTest() {
        long start;
        int numItems = 800;
        int numTests = 10;
        for (int j = 0; j < numTests; j++) {
            start = System.currentTimeMillis();
            for (int i = 0; i < numItems; i++) {
                highScores.add(new Player("FabianPriorityQueue", i + "", getHighScore()));
            }
            System.out.println((System.currentTimeMillis() - start));
            //create fresh instance of currently using highscores.
            createHighScores();
        }
    }

    @Test
    public void testSmall() {
        highScores.add(new Player("test1", "test1", 1));
        highScores.add(new Player("test1", "test1", 2));
        highScores.add(new Player("test1", "test1", 3));
        highScores.add(new Player("test1", "test1", 6));
        highScores.add(new Player("test1", "test1", 4));
        highScores.add(new Player("test1", "test1", 5));
        List<Player> output = highScores.getHighScores(6);
        assertEquals(6, output.get(0).getHighScore());
    }

    @Test
    public void findPlayer() {
        highScores.add(new Player("Fabian", "Lockhorst", 1));
        highScores.add(new Player("Fabian", "Straat", 2));
        highScores.add(new Player("Joost", "Lockhorst", 3));
        highScores.add(new Player("Niks", "Nooit", 4));
        assertEquals(3, highScores.findPlayer("Fabian", "Lockhorst").size());
    }

    @Test(expected = IllegalArgumentException.class)
    public void failFindPlayer() {
        highScores.findPlayer("", "");
    }

    @Test
    public void correctAmountOfHighScoresShouldBeGiven() {
        highScores.add(nearlyHeadlessNick);
        highScores.add(dumbledore);
        highScores.add(new Player("test", "potter", getHighScore()));

        assertEquals(2, highScores.getHighScores(2).size());
    }
}